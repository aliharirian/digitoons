import React from "react";

import { aside2, menu, menubtn, logo, line, homebtn } from "./menu.styl";

import Menubtn from "./Menubtn.jsx";

const Menu = props => {
  

  return (
    <div className={aside2}>
      <div className={menu}>
        <div className={menubtn}>
          <img className={logo} src={require("./assets/logo.png")} />
        </div>
        {props.items.map((item) => {
          return (
            <React.Fragment>
              <svg className={line}>
                <line x1="25" y1="0" x2="25" y2="50" />
              </svg>

              <Menubtn className={menubtn}>
                {item}
              </Menubtn>
            </React.Fragment>
          )
        })}
      </div>
    </div>
  );
};

export default Menu;
