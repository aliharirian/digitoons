import React from "react";
import { hot } from "react-hot-loader";
import {
  main,
  mainParent,
  aside1,
  aside2,
  footer,
  wrapper,
  mainText
} from "./App.styl";

import Moviecontainer from "./Moviecontainer.jsx";
import Main from "./Main.jsx";
import Menu from "./Menu.jsx";
import image from "./movies/1.jpg";
import image2 from "./movies/2.jpg";
import image3 from "./movies/3.jpg";
import image4 from "./movies/4.jpg";
import image5 from "./movies/5.jpg";


/* var myImage = new Image(100, 200);
myImage.src = './movies/1.jpg';

var imgArray = new Array();

imgArray[0] = new Image();
imgArray[0].src = './movies/1.jpg';
*/

const App = () => {
  console.log("hi");
  return (
    
    <div className={mainParent}>
      <div className={wrapper}>
        <Moviecontainer x={image} images={[image, image2, image3, image4, image5]} className={aside1} />

        <Main className={main} />
        <Menu
          items={["خانه", "محصولات", "تماس با ما", "درباره ما"]}
          className={aside2}
        />

        <div className={footer} />
      </div>
    </div>
  );
};

export default hot(module)(App);
