import React from "react";

import {
  drawerbtn,
  movieimage,
  moviecontainer,
  moviebox,
  aside1
  
} from "./moviecontainer.styl";

import Movie from "./Movie.jsx"

const Moviecontainer = (props) => {
  console.log(props);
  
  return (

<div className={aside1}>
          <div className={moviecontainer}>
            <div className={drawerbtn}>
              <img src={require("./assets/drawerbtn.svg")} />
            </div>
            {props.images.map((image) => {
          return (
            <Movie className={moviebox}>{image}</Movie>
          )
        })}
            
          </div>
          </div>
        
  )};

export default Moviecontainer;
