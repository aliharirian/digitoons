import React from "react";

import {
main,
maininside,
characterbox,
shadow,
pointingline,
dialogcontainer,
dialogbox,
dialog,
character,
backgroundLayout,

  
} from "./main.styl";

const Main = (props) => {
  console.log(props);
  
  return (

<div className={main}>
          <div className={maininside}>
            <div className={characterbox}>
              <img className={shadow} src={require("./assets/shadow1.png")} />
              <img
                className={pointingline}
                src={require("./assets/pointingline.svg")}
              />
              <div className={dialogcontainer}>
                <img
                  className={dialogbox}
                  src={require("./assets/dialogbox.svg")}
                />
                <p className={dialog}>برای دیدن انیمیشن های بیشتر برید اینجا</p>
              </div>
              <img
                className={character}
                src={require("./assets/character.svg")}
              />
            </div>
            <img className={backgroundLayout} src={require("./assets/background.svg")} />
          </div>
        </div>
          )};

export default Main;
          