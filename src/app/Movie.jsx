import React from "react";

import { movieimage, moviebox } from "./movie.styl";

const Movie = props => {
  return (

  <div className={moviebox}>

  <img className={movieimage} src={props.children}/>

  </div>
)};

export default Movie;
